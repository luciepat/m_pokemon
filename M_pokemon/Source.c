#include <stdio.h>
#include "pokemon.h"

int main() {
	pokemon bulb = initialize("Bulbasaur", 1000, 200, Fire);
	pokemon digglet = initialize("digglet", 1000, 200, Earth);
	who_wins(bulb, digglet);
	getchar();
	return 0;
}