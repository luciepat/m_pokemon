#include "pokemon.h"

/*
returns the effectiveness. Basically the upper hand gets X2 and the lower hand gets /2. 
so in effectiveness I give the upper X4
in: pokemon and opponent pokemon
out: effective mutltiplier
*/
size_t effective(pokemon _self, pokemon _opponent)
{
	if ((_self._type == Fire && _opponent._type == Air) ||
		(_self._type == Water && _opponent._type == Fire) ||
		(_self._type == Air && _opponent._type == Earth) ||
		(_self._type == Earth && _opponent._type == Water)){
		return 4;
	}
	else {
		return 1;
	}

}


/*
gets 2 poke and decides who wins
*/
void who_wins(pokemon first, pokemon second)
{
	if (!(is_init(first) && is_init(second)))
	{
		printf("Uninitialized pokemon");
		return;
	}
	size_t fPower, sPower;
	fPower = (first._attack / second._defense)*effective(first, second) * 50;
	sPower = (second._attack / first._defense)*effective(second, first) * 50;
	if (fPower > sPower) {
		printf("%s will probably win", first._name);
	}
	else if (fPower == sPower) {
		printf("Too close to tell");
	}
	else {
		printf("%s will probably win", second._name);
	}

}

/*
creates pokemon
in: Name, strength, defense, type
out pokemon 
*/
pokemon initialize(char * name, size_t str, size_t def, size_t type)
{
	pokemon _new;
	_new._name = name;
	_new._attack = str;
	_new._defense = def;
	if (type<4 && type>=0) {
		_new._type = type;
		return _new;
	}
	printf("Invalid Init");
	return _new;
}

/*
checks if pokemon is init
1 if yes, 0 if not
*/
int is_init(pokemon _check)
{
	if (_check._name && _check._attack && _check._defense && (_check._type>=0 && _check._type <4)) {
		return 1;
	}
	return 0;
}
