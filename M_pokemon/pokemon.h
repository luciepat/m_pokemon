#pragma once
#include <stdio.h>
enum Type {
	Fire,
	Water,
	Earth,
	Air
};

typedef struct pokemon {
	size_t _attack;
	size_t _defense;
	size_t _type;
	char* _name;
} pokemon;

size_t effective(pokemon _self, pokemon _opponent);
void who_wins(pokemon first, pokemon second);
pokemon initialize(char* name, size_t str, size_t def, size_t type);
int is_init(pokemon _check);